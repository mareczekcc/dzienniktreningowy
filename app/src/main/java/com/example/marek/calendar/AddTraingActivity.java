package com.example.marek.calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddTraingActivity extends Activity {

    private EditText dataReceived, txtDate, txtKm, txtTime;
    private Button btnDone;
    private Spinner listaR, listaT;
    private LinearLayout layoutT;
    private int year;
    private int month;
    private int day;
    private String tag="";

    DatePickerDialog.OnDateSetListener date;
    Calendar myCalendar = Calendar.getInstance();

    List<EditText> allEds = new ArrayList<EditText>();
    LinearLayout.LayoutParams paramsLayout = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
    LinearLayout.LayoutParams paramsEdit = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,1);
    LinearLayout.LayoutParams paramsText = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,5);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main2);
        setContentView(R.layout.trening_layout);
        txtDate = (EditText) findViewById(R.id.txtDate);
        txtKm = (EditText) findViewById(R.id.txtKm);
        txtTime = (EditText) findViewById(R.id.txtTime);
        btnDone = (Button) findViewById(R.id.btnDone);
        dataReceived = (EditText) findViewById(R.id.etDataReceived);
        listaR = (Spinner) findViewById(R.id.listR);
        listaT = (Spinner) findViewById(R.id.listT);
        layoutT = (LinearLayout)findViewById(R.id.layoutTrening);

        setCurrentDateOnView();
        setOnDateChange();
        setTxtOnClickListener();
        getBundle();

        listaT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                layoutT.removeAllViews();
                if(listaT.getSelectedItemPosition()==1) {
                    //layoutT.setWeightSum(13.5F);
                    layoutT.removeAllViews();
                    allEds =setNogi(layoutT);

                }else if(listaT.getSelectedItemPosition()==2) {
                   // layoutT.setWeightSum(13.5F);
                    layoutT.removeAllViews();
                    allEds =setKlatka(layoutT);
                }else if(listaT.getSelectedItemPosition()==3){
                    //layoutT.setWeightSum(15.0F);
                    layoutT.removeAllViews();
                    allEds = setPlecy(layoutT);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                layoutT.removeAllViews();
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {

                DBHandler db = new DBHandler(AddTraingActivity.this);

                //Pobieranie danych z EditTextów
                String data = txtDate.getText().toString();
                String typeW = listaR.getSelectedItem().toString();
                int km=0;
                int time =0;
                if(!txtKm.getText().toString().matches("")) {
                    km = Integer.parseInt(txtKm.getText().toString());
                }
                if(!txtTime.getText().toString().matches("")) {
                    time = Integer.parseInt(txtTime.getText().toString());
                }
                String typeT = listaT.getSelectedItem().toString();

                String weights="";
                for (int i =0; i<allEds.size()-1;i++){
                    if(TextUtils.isEmpty(allEds.get(i).getText())) {
                        weights += "0,";
                    }else
                    weights+=allEds.get(i).getText()+",";
                }
                if(TextUtils.isEmpty(allEds.get(allEds.size()-1).getText())) {
                    weights += "0";
                }else
                weights+=allEds.get(allEds.size()-1).getText();

                String text = data+typeW+Integer.toString(km)+Integer.toString(time)+typeT+weights;

                db.addTraining(new Training(data,typeW,typeT,km,time,weights));
                List<Training> trainings = db.getTrainings();

                Intent myLocalIntent = getIntent();
                Bundle myBundle = myLocalIntent.getExtras();
                // add to the bundle the computed result
                //String text = txtDate.getText().toString();
                myBundle.putString("text",text);
                myLocalIntent.putExtras(myBundle);

                setResult(Activity.RESULT_OK, myLocalIntent);

                finish();
            }
        });

    }//onCreate

    private List<EditText> setPlecy(LinearLayout layoutT) {

        List<EditText> allEds = new ArrayList<EditText>();

        DBHandler db = new DBHandler(AddTraingActivity.this);
        Training training = db.getLastTraining("Plecy+Ramiona");

        String[] wagi = training.getWeights().split(",");

        String[] tab = {"Martwy ciąg","Wiosłowanie sztangą","Podciąganie hantla w opadzie",
                "Przyciąganie drążka z wyciągu górnego","Przyciąganie drążka z wyciągu dolnego",
                "Wyciskanie hantli siedząc","Arnoldki","Odwodzenie ramion w bok","Unoszenie ramion w przód","Szrugsy"};


        for(int i=0;i<10;i++){

            LinearLayout l = new LinearLayout(getBaseContext());
            TextView t = new TextView(getBaseContext());
            EditText e = new EditText(getBaseContext());
            TextView o = new TextView(getBaseContext());

            l.setOrientation(LinearLayout.HORIZONTAL);
            l.setLayoutParams(paramsLayout);
            l.setWeightSum(7);

            t.setText(tab[i]);
            t.setLayoutParams(paramsText);

            e.setLayoutParams(paramsEdit);
            e.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            e.setId(View.generateViewId());

            o.setText(wagi[i]+"kg");
            o.setLayoutParams(paramsEdit);

            l.addView(t);
            l.addView(e);
            l.addView(o);

            layoutT.addView(l);

            allEds.add(e);
        }
        return allEds;
    }

    private List<EditText> setKlatka(LinearLayout layoutT) {

        List<EditText> allEds = new ArrayList<EditText>();

        DBHandler db = new DBHandler(AddTraingActivity.this);
        Training training = db.getLastTraining("Klatka+Triceps");

        String[] wagi = training.getWeights().split(",");

        String[] tab = {"Wyciskanie na ławce płaskiej","Wyciskanie na ławce do góry",
                "Wyciskanie na ławce w dół","Podnoszenie talerza","Rozpiętki","Francuskie wyciskanie",
                "Wyprosty rąk w opadzie","Wyciskanie sztangi bliskim chwytem","Wyprosty rąk na wyciągu górnym"};

        for(int i=0;i<9;i++){

            LinearLayout l = new LinearLayout(getBaseContext());
            TextView t = new TextView(getBaseContext());
            EditText e = new EditText(getBaseContext());
            TextView o = new TextView(getBaseContext());

            l.setOrientation(LinearLayout.HORIZONTAL);
            l.setLayoutParams(paramsLayout);
            l.setWeightSum(7);

            t.setText(tab[i]);
            t.setLayoutParams(paramsText);

            e.setLayoutParams(paramsEdit);
            e.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            e.setId(View.generateViewId());

            o.setText(wagi[i]+"kg");
            o.setLayoutParams(paramsEdit);

            l.addView(t);
            l.addView(e);
            l.addView(o);

            layoutT.addView(l);

            allEds.add(e);
        }
        return allEds;
    }

    private List<EditText> setNogi(LinearLayout layoutT) {
        List<EditText> allEds = new ArrayList<EditText>();

        DBHandler db = new DBHandler(AddTraingActivity.this);
        Training training = db.getLastTraining("Nogi+Biceps");

        String[] wagi = training.getWeights().split(",");

        String[] tab = {"Przysiady ze sztangą","Wykroki z hantlami","Wypychanie nóg na suwnicy",
                "Wyprosty nóg siedząc","Zginanie podudzi","Wspięcia na palce","Podciąganie hantli naprzemiennie",
                "Podciąganie sztangi podchwytem siedząc","Podciąganie sztangi podchwytem stojąc"};

        for(int i=0;i<9;i++){

            LinearLayout l = new LinearLayout(getBaseContext());
            TextView t = new TextView(getBaseContext());
            EditText e = new EditText(getBaseContext());
            TextView o = new TextView(getBaseContext());

            l.setOrientation(LinearLayout.HORIZONTAL);
            l.setLayoutParams(paramsLayout);
            l.setWeightSum(7);

            t.setText(tab[i]);
            t.setLayoutParams(paramsText);

            e.setLayoutParams(paramsEdit);
            e.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
            e.setId(View.generateViewId());

            o.setText(wagi[i]+"kg");
            o.setLayoutParams(paramsEdit);

            l.addView(t);
            l.addView(e);
            l.addView(o);

            layoutT.addView(l);

            allEds.add(e);
        }
        return allEds;
    }

    private void setTxtOnClickListener() {

        txtDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(AddTraingActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void setOnDateChange() {
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
    }

    public void getBundle(){
        Intent myLocalIntent = getIntent();
        Bundle myBundle = myLocalIntent.getExtras();
        tag = myBundle.getString("tag");
        txtDate.setText(tag);

    }

    public void setCurrentDateOnView() {

        txtDate = (EditText) findViewById(R.id.txtDate);

        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);


        txtDate.setText(new StringBuilder()
                .append(year).append(".0").append(month + 1).append(".").append(day).append(" "));
    }
    private void updateLabel() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

        txtDate.setText(sdf.format(myCalendar.getTime()));
    }

}