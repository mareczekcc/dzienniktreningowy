package com.example.marek.calendar;

import java.util.ArrayList;

/**
 * Created by Marek on 27.05.2017.
 */

public class Training {
    private int id;
    private String date;
    private String typeW;
    private String typeT;
    private int km;
    private int time;

    private String weights;

    public Training() {
    }

    public Training(String date, String typeW, String typeT, int km, int time, String weights) {
        this.id = 0;
        this.date = date;
        this.typeW = typeW;
        this.typeT = typeT;
        this.km = km;
        this.time = time;
        this.weights = weights;
    }
    public Training(int id) {
        this.id = id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTypeW(String typeW) {
        this.typeW = typeW;
    }

    public void setTypeT(String typeT) {
        this.typeT = typeT;
    }

    public void setKm(int km) {
        this.km = km;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setWeights(String weights) {
        this.weights = weights;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getTypeW() {
        return typeW;
    }

    public String getTypeT() {
        return typeT;
    }

    public int getKm() {
        return km;
    }

    public int getTime() {
        return time;
    }

    public String getWeights() {
        return weights;
    }
}