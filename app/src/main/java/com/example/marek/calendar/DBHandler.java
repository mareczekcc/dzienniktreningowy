package com.example.marek.calendar;

/**
 * Created by Marek on 27.05.2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DBHandler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "TrainingsDB";
    // Contacts table name
    private static final String TABLE_TRAINING = "training";
    // Shops Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_DATE = "date";
    private static final String KEY_TYPEW = "typeW";
    private static final String KEY_TYPET = "typeT";
    private static final String KEY_KM = "km";
    private static final String KEY_TIME = "time";
    private static final String KEY_WEIGHTS = "weights";

    public DBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_TRAINING + "("
        + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_DATE + " DATE,"
        + KEY_TYPEW + " TEXT," +KEY_TYPET+" TEXT,"+KEY_KM +" INTEGER,"+KEY_TIME+" INTEGER,"
                +KEY_WEIGHTS+" TEXT"+")";
        db.execSQL(CREATE_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
// Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAINING);
// Creating tables again
        onCreate(db);
    }

    public void addTraining(Training training) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DATE, training.getDate());
        values.put(KEY_TYPEW, training.getTypeW());
        values.put(KEY_TYPET, training.getTypeT());
        values.put(KEY_KM, training.getKm());
        values.put(KEY_TIME, training.getTime());
        values.put(KEY_WEIGHTS, training.getWeights());

// Inserting Row
        db.insert(TABLE_TRAINING, null, values);
        db.close(); // Closing database connection
    }

    public List<Training> getWhereClause(String type) {
        List<Training> trainingList = new ArrayList<Training>();
// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_TRAINING + " WHERE typeT='"+type+"' ORDER BY date DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Training training = new Training();
                training.setId(Integer.parseInt(cursor.getString(0)));
                training.setDate(cursor.getString(1));
                training.setTypeW(cursor.getString(2));
                training.setTypeT(cursor.getString(3));
                training.setKm(Integer.parseInt(cursor.getString(4)));
                training.setTime(Integer.parseInt(cursor.getString(5)));
                training.setWeights(cursor.getString(6));
// Adding contact to list
                trainingList.add(training);
            } while (cursor.moveToNext());
        }

// return contact list
        return trainingList;
    }

    public List<Training> getWhereDate(String type) {
        List<Training> trainingList = new ArrayList<Training>();
// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_TRAINING + " WHERE date='"+type+"' ORDER BY date DESC LIMIT 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Training training = new Training();
                training.setId(Integer.parseInt(cursor.getString(0)));
                training.setDate(cursor.getString(1));
                training.setTypeW(cursor.getString(2));
                training.setTypeT(cursor.getString(3));
                training.setKm(Integer.parseInt(cursor.getString(4)));
                training.setTime(Integer.parseInt(cursor.getString(5)));
                training.setWeights(cursor.getString(6));
// Adding contact to list
                trainingList.add(training);
            } while (cursor.moveToNext());
        }

// return contact list
        return trainingList;
    }


    public Training getLastTraining(String type) {

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_TRAINING + " WHERE typeT='"+type+"' ORDER BY date DESC LIMIT 1";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null)
            cursor.moveToFirst();
        Training training = new Training();
        training.setId(Integer.parseInt(cursor.getString(0)));
        training.setDate(cursor.getString(1));
        training.setTypeW(cursor.getString(2));
        training.setTypeT(cursor.getString(3));
        training.setKm(Integer.parseInt(cursor.getString(4)));
        training.setTime(Integer.parseInt(cursor.getString(5)));
        training.setWeights(cursor.getString(6));
// return shop
        return training;
    }

    public List<Training> getTrainings() {
        List<Training> trainingList = new ArrayList<Training>();
// Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_TRAINING;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

// looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Training training = new Training();
                training.setId(Integer.parseInt(cursor.getString(0)));
                training.setDate(cursor.getString(1));
                training.setTypeW(cursor.getString(2));
                training.setTypeT(cursor.getString(3));
                training.setKm(Integer.parseInt(cursor.getString(4)));
                training.setTime(Integer.parseInt(cursor.getString(5)));
                training.setWeights(cursor.getString(6));
// Adding contact to list
                trainingList.add(training);
            } while (cursor.moveToNext());
        }

// return contact list
        return trainingList;
    }

    // Deleting a shop
    public void deleteTraining(Training training) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TRAINING, KEY_ID + " = ?",
        new String[] { String.valueOf(training.getId()) });
        db.close();
    }

}
