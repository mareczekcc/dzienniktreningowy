package com.example.marek.calendar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.app.Activity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


//Android 19


public class MainActivity extends Activity {
    Button toAddTr;
    Button toSeeTr;
    Button toCalendar;
    Button toDelete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //sets the main layout of the activity
        setContentView(R.layout.activity_main);

        toAddTr =(Button)findViewById(R.id.btnAddT);
        toSeeTr =(Button)findViewById(R.id.btnCheckT);
        toCalendar=(Button)findViewById(R.id.btnCalendar);
        toDelete=(Button)findViewById(R.id.btnDelete);

        toAddTr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
                Date today = Calendar.getInstance().getTime();
                String tag = sdf.format(today);

                Intent myIntentA1A2 = new Intent (MainActivity.this, AddTraingActivity.class);
                Bundle myDataBundle = new Bundle();

                myDataBundle.putString("tag",tag);

                myIntentA1A2.putExtras(myDataBundle);

                startActivityForResult(myIntentA1A2, 101);
            }
        });

        toSeeTr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), DoneTrainings.class);
                view.getContext().startActivity(intent);
            }

        });

        toDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), DeleteTraining.class);
                view.getContext().startActivity(intent);
            }

        });
        toCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(view.getContext(), CalendarActivity.class);
                Intent intent = new Intent(view.getContext(), CalendarActivity.class);
                view.getContext().startActivity(intent);
            }

        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if ((requestCode == 101 ) && (resultCode == Activity.RESULT_OK)){
                Bundle myResultBundle = data.getExtras();
            }
        }
        catch (Exception e) {

        }
    }//onActivityResult

}