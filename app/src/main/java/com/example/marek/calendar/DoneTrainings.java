package com.example.marek.calendar;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class DoneTrainings extends Activity {

    private Button btnDone;
    private Spinner listaT;
    private LinearLayout layoutT;

    DBHandler db = new DBHandler(DoneTrainings.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done_trainings);


        btnDone = (Button) findViewById(R.id.btnDone);
        listaT = (Spinner) findViewById(R.id.listT);
        layoutT = (LinearLayout)findViewById(R.id.layoutTrening);

        listaT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                layoutT.removeAllViews();
                if(listaT.getSelectedItemPosition()==1) {
                    layoutT.removeAllViews();
                    findTraining("Nogi+Biceps",layoutT);
                }else if(listaT.getSelectedItemPosition()==2) {
                    layoutT.removeAllViews();
                    findTraining("Klatka+Triceps",layoutT);
                }else if(listaT.getSelectedItemPosition()==3){
                    layoutT.removeAllViews();
                    findTraining("Plecy+Ramiona",layoutT);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //layoutT.removeAllViews();
            }
        });


        btnDone.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {

                finish();
            }
        });


    }

    private void findTraining(String type,LinearLayout layoutT) {
        LinearLayout.LayoutParams paramsLayout = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        LinearLayout.LayoutParams paramsMedium = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,2);
        LinearLayout.LayoutParams paramsBig= new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,4);
        LinearLayout.LayoutParams paramsSmall = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,0.5F);

        List<Training> trainings = db.getWhereClause(type);

        //Nagłówek
        TextView naglowek = new TextView(getBaseContext());
        naglowek.setText("Data:              Typ Rozg.: Km:  T:  Wagi:");
        layoutT.addView(naglowek);
        for(int i=0; i<trainings.size();i++){
            LinearLayout l = new LinearLayout(getBaseContext());

            l.setOrientation(LinearLayout.HORIZONTAL);
            l.setLayoutParams(paramsLayout);
            l.setWeightSum(9);

            TextView data = new TextView(getBaseContext());
            TextView rozgrzewka = new TextView(getBaseContext());
            TextView km = new TextView(getBaseContext());
            TextView time = new TextView(getBaseContext());
            TextView wagi = new TextView(getBaseContext());

            data.setText(trainings.get(i).getDate());
            rozgrzewka.setText(trainings.get(i).getTypeW());
            km.setText(Integer.toString(trainings.get(i).getKm()));
            time.setText(Integer.toString(trainings.get(i).getTime()));
            wagi.setText(trainings.get(i).getWeights());

            data.setLayoutParams(paramsMedium);
            rozgrzewka.setLayoutParams(paramsMedium);
            km.setLayoutParams(paramsSmall);
            time.setLayoutParams(paramsSmall);
            wagi.setLayoutParams(paramsBig);

            l.addView(data);
            l.addView(rozgrzewka);
            l.addView(km);
            l.addView(time);
            l.addView(wagi);

            layoutT.addView(l);

        }

    }
}
