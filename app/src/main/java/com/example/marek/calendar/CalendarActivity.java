package com.example.marek.calendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class CalendarActivity extends Activity implements OnClickListener
	{
		private static final String tag = "Main";
		private Button selectedDayMonthYearButton;
		private Button currentMonth;
        private TextView textLegenda;
		private ImageView prevMonth;
		private ImageView nextMonth;
		private GridView calendarView;
		private GridCellAdapter adapter;
		private Calendar _calendar;
		private int month, year;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        DBHandler db = new DBHandler(CalendarActivity.this);

		private void getRequestParameters()
			{
				Intent intent = getIntent();
				if (intent != null)
					{
						Bundle extras = intent.getExtras();
						if (extras != null)
							{
								if (extras != null)
									{
										Log.d(tag, "+++++----------------->" + extras.getString("params"));
									}
							}
					}
			}

		/** Called when the activity is first created. */
		@Override
		public void onCreate(Bundle savedInstanceState)
			{
				super.onCreate(savedInstanceState);
				setContentView(R.layout.main);

                textLegenda = (TextView) this.findViewById(R.id.textLegenda);

                Spannable word = new SpannableString("Plecy+Ramiona ");
                word.setSpan(new ForegroundColorSpan(Color.BLUE), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                textLegenda.setText(word);

                Spannable wordTwo = new SpannableString("Klatka+Triceps ");
                wordTwo.setSpan(new ForegroundColorSpan(Color.RED), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                textLegenda.append(wordTwo);

                Spannable wordThree = new SpannableString("Nogi+Biceps");
                wordThree.setSpan(new ForegroundColorSpan(Color.GREEN), 0, wordThree.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                textLegenda.append(wordThree);

				_calendar = Calendar.getInstance(Locale.getDefault());
				month = _calendar.get(Calendar.MONTH);
				year = _calendar.get(Calendar.YEAR);

				//selectedDayMonthYearButton = (Button) this.findViewById(R.id.selectedDayMonthYear);

				prevMonth = (ImageView) this.findViewById(R.id.prevMonth);
				prevMonth.setOnClickListener(this);

				currentMonth = (Button) this.findViewById(R.id.currentMonth);
				currentMonth.setText(_calendar.getTime().toString());

				nextMonth = (ImageView) this.findViewById(R.id.nextMonth);
				nextMonth.setOnClickListener(this);

				calendarView = (GridView) this.findViewById(R.id.calendar);

				// Initialised
				adapter = new GridCellAdapter(getApplicationContext(), R.id.gridcell, month, year);
				adapter.notifyDataSetChanged();
				calendarView.setAdapter(adapter);

			}

		@Override
		public void onClick(View v)
			{
				if (v == prevMonth)
					{
						if (month <= 1)
							{
								month = 11;
								year--;
							} else
							{
								month--;
							}

						Log.d(tag, "Before 1 MONTH " + "Month: " + month + " " + "Year: " + year);
						adapter = new GridCellAdapter(getApplicationContext(), R.id.gridcell, month, year);
						_calendar.set(year, month, _calendar.get(Calendar.DAY_OF_MONTH));

						adapter.notifyDataSetChanged();
						calendarView.setAdapter(adapter);
					}
				if (v == nextMonth)
					{
						if (month >= 11)
							{
								month = 0;
								year++;
							} else
							{
								month++;
							}

						Log.d(tag, "After 1 MONTH " + "Month: " + month + " " + "Year: " + year);
						adapter = new GridCellAdapter(getApplicationContext(), R.id.gridcell, month, year);
						_calendar.set(year, month, _calendar.get(Calendar.DAY_OF_MONTH));
						adapter.notifyDataSetChanged();
						calendarView.setAdapter(adapter);
					}
			}

		//
		public class GridCellAdapter extends BaseAdapter implements OnClickListener
			{
				private static final String tag = "GridCellAdapter";
				private final Context _context;
				private final List<String> list;
				private final String[] weekdays = new String[] { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
				private final String[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
				private final int[] daysOfMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
				private final int month, year;
				int daysInMonth, prevMonthDays;
				private final int currentDayOfMonth;
				private Button gridcell;

				// Days in Current Month
				public GridCellAdapter(Context context, int textViewResourceId, int month, int year)
					{
						super();
						this._context = context;
						this.list = new ArrayList<String>();
						this.month = month;
						this.year = year;

						Log.d(tag, "Month: " + month + " " + "Year: " + year);
						Calendar calendar = Calendar.getInstance();
						currentDayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
						currentMonth.setText(months[month]);

						printMonth(month, year);
					}

				public String getItem(int position)
					{
						return list.get(position);
					}

				@Override
				public int getCount()
					{
						return list.size();
					}

				private void printMonth(int mm, int yy)
					{
						// The number of days to leave blank at
						// the start of this month.
						int trailingSpaces = 0;
						int leadSpaces = 0;
						int daysInPrevMonth = 0;
						int prevMonth = 0;
						int prevYear = 0;
						int nextMonth = 0;
						int nextYear = 0;

						GregorianCalendar cal = new GregorianCalendar(yy, mm, currentDayOfMonth);

						// Days in Current Month
						daysInMonth = daysOfMonth[mm];
						int currentMonth = mm;
						if (currentMonth == 11)
							{
								prevMonth = 10;
								daysInPrevMonth = daysOfMonth[prevMonth];
								nextMonth = 0;
								prevYear = yy;
								nextYear = yy + 1;
							} else if (currentMonth == 0)
							{
								prevMonth = 11;
								prevYear = yy - 1;
								nextYear = yy;
								daysInPrevMonth = daysOfMonth[prevMonth];
								nextMonth = 1;
							} else
							{
								prevMonth = currentMonth - 1;
								nextMonth = currentMonth + 1;
								nextYear = yy;
								prevYear = yy;
								daysInPrevMonth = daysOfMonth[prevMonth];
							}

						// Compute how much to leave before before the first day of the
						// month.
						// getDay() returns 0 for Sunday.
						trailingSpaces = cal.get(Calendar.DAY_OF_WEEK);

						if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 1)
							{
								++daysInMonth;
							}

						// Trailing Month days
						for (int i = 0; i < trailingSpaces; i++)
							{
								list.add(String.valueOf((daysInPrevMonth - trailingSpaces + 1) + i) + "-GREY" + "-" + months[prevMonth] + "-" + prevYear);
							}

						// Current Month Days
						for (int i = 1; i <= daysInMonth; i++)
							{
								list.add(String.valueOf(i) + "-WHITE" + "-" + months[mm] + "-" + yy);
							}

						// Leading Month days
						for (int i = 0; i < list.size() % 7; i++)
							{
								Log.d(tag, "NEXT MONTH:= " + months[nextMonth]);
								list.add(String.valueOf(i + 1) + "-GREY" + "-" + months[nextMonth] + "-" + nextYear);
							}
					}

				@Override
				public long getItemId(int position)
					{
						return position;
					}

				@Override
				public View getView(int position, View convertView, ViewGroup parent)
					{
						Log.d(tag, "getView ...");
						View row = convertView;
						if (row == null)
							{
								// ROW INFLATION
								Log.d(tag, "Starting XML Row Inflation ... ");
								LayoutInflater inflater = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
								row = inflater.inflate(R.layout.gridcell, parent, false);

								Log.d(tag, "Successfully completed XML Row Inflation!");
							}

						gridcell = (Button) row.findViewById(R.id.gridcell);
						gridcell.setOnClickListener(this);

						// ACCOUNT FOR SPACING

						Log.d(tag, "Current Day: " + currentDayOfMonth);
						String[] day_color = list.get(position).split("-");

						gridcell.setText(day_color[0]);


						///////////////////////////////////////////////////////////////////////////////////////////Zmiana formatu daty
						SimpleDateFormat format = new SimpleDateFormat("MMMM d, yyyy", Locale.ENGLISH);
						String data = day_color[2]+" "+day_color[0]+", "+day_color[3];
						Date date;
						String newDate="";
						try {
							date = format.parse(data);
							newDate = sdf.format(date);
						} catch (ParseException e) {
							e.printStackTrace();
						}
						gridcell.setTag(newDate);

						//////////////////////////////////////////////////////////////////////////////////////////Koniec

						if (day_color[1].equals("GREY"))
							{
								gridcell.setTextColor(Color.LTGRAY);
							}
						if (day_color[1].equals("WHITE"))
							{
								gridcell.setTextColor(Color.WHITE);
							}
						if (position == currentDayOfMonth)
							{
								//gridcell.setTextColor(Color.BLUE);
							}

							sprawdzTreningi(newDate);


						return row;
					}
						/////////////////////////////////////////////////////////////////////////////////////////Moja metoda
                        private void sprawdzTreningi(String data) {
                            String miesiac = data.split("\\.")[1];
                            String dzien = data.split("\\.")[2];

                            List<Training> trainings = db.getWhereDate(data);

                            if(trainings.size()>0){
                                if(trainings.get(0).getTypeT().equals("Plecy+Ramiona"))
                                    gridcell.setTextColor(Color.BLUE);
                                if(trainings.get(0).getTypeT().equals("Klatka+Triceps"))
                                    gridcell.setTextColor(Color.RED);
                                if(trainings.get(0).getTypeT().equals("Nogi+Biceps"))
                                    gridcell.setTextColor(Color.GREEN);
                            }
                }
						//////////////////////////////////////////////////////////////////////////////////////////Koniec
                @Override
				public void onClick(View view)
					{
						String date_month_year = (String) view.getTag();

						Calendar calendar = Calendar.getInstance();
						int year = calendar.get(Calendar.YEAR);
						int month = calendar.get(Calendar.MONTH);
						int day = calendar.get(Calendar.DAY_OF_MONTH);

						Intent myIntentA1A2 = new Intent (CalendarActivity.this, AddTraingActivity.class);
						Bundle myDataBundle = new Bundle();

						myDataBundle.putInt("year", year);
						myDataBundle.putInt("month", month+1);
						myDataBundle.putInt("day", day);
						myDataBundle.putString("tag", date_month_year);

						myIntentA1A2.putExtras(myDataBundle);

						startActivityForResult(myIntentA1A2, 101);
					}
			}

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            try {
                if ((requestCode == 101 ) && (resultCode == Activity.RESULT_OK)){
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            }
            catch (Exception e) {

            }
        }//onActivityResult
	}

