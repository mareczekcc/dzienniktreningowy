package com.example.marek.calendar;

import android.app.Activity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class DeleteTraining extends Activity {

    private Button btnDone;
    private Spinner listaT;
    private LinearLayout layoutT;

    DBHandler db = new DBHandler(DeleteTraining.this);
    List<Integer> checkBoxes = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done_trainings);



        btnDone = (Button) findViewById(R.id.btnDone);
        listaT = (Spinner) findViewById(R.id.listT);
        layoutT = (LinearLayout)findViewById(R.id.layoutTrening);

        //List<Training> trainings = db.getTrainings();
        //List<Training> trainings = db.getWhereClause(type);

        listaT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                layoutT.removeAllViews();

                if(listaT.getSelectedItemPosition()==1) {
                    layoutT.removeAllViews();
                    checkBoxes=findTraining("Nogi+Biceps",layoutT);
                }else if(listaT.getSelectedItemPosition()==2) {
                    layoutT.removeAllViews();
                    checkBoxes=findTraining("Klatka+Triceps",layoutT);
                }else if(listaT.getSelectedItemPosition()==3){
                    layoutT.removeAllViews();
                    checkBoxes=findTraining("Plecy+Ramiona",layoutT);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //layoutT.removeAllViews();
            }
        });


        btnDone.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v) {
                int temp;
                CheckBox check;
                List<CheckBox> checked = new ArrayList<CheckBox>();
                for(int i=0; i<checkBoxes.size();i++){
                    temp = getResources().getIdentifier(Integer.toString(checkBoxes.get(i)), "id", getPackageName());
                    check = (CheckBox) findViewById(temp);
                    if(check.isChecked()){
                        checked.add(check);
                    }
                }


                for(int i=0; i<checked.size();i++){

                    temp = checked.get(i).getId();
db.deleteTraining(new Training(temp));
                }

                finish();
            }
        });


    }

    private List<Integer> findTraining(String type,LinearLayout layoutT) {
        LinearLayout.LayoutParams paramsLayout = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT);
        LinearLayout.LayoutParams paramsData = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,2);
        LinearLayout.LayoutParams paramsOther = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT,1);

        //List<Training> trainings = db.getTrainings();
        List<Training> trainings = db.getWhereClause(type);

        //Nagłówek
        TextView naglowek = new TextView(getBaseContext());
        naglowek.setText("Zaznacz:          Data:             Typ rozg.:          Km:    Czas:");
        layoutT.addView(naglowek);
        List<Integer> checkBoxes = new ArrayList<Integer>();

        for(int i=0; i<trainings.size();i++){
            LinearLayout l = new LinearLayout(getBaseContext());

            l.setOrientation(LinearLayout.HORIZONTAL);
            l.setLayoutParams(paramsLayout);
            l.setWeightSum(8);

            CheckBox check = new CheckBox(getBaseContext());
            TextView data = new TextView(getBaseContext());
            TextView rozgrzewka = new TextView(getBaseContext());
            TextView km = new TextView(getBaseContext());
            TextView time = new TextView(getBaseContext());

            data.setText(trainings.get(i).getDate());
            rozgrzewka.setText(trainings.get(i).getTypeW());
            km.setText(Integer.toString(trainings.get(i).getKm()));
            time.setText(Integer.toString(trainings.get(i).getTime()));

            check.setLayoutParams(paramsData);
            data.setLayoutParams(paramsData);
            rozgrzewka.setLayoutParams(paramsData);
            km.setLayoutParams(paramsOther);
            time.setLayoutParams(paramsOther);

            check.setId(trainings.get(i).getId());

            checkBoxes.add(trainings.get(i).getId());

            l.addView(check);
            l.addView(data);
            l.addView(rozgrzewka);
            l.addView(km);
            l.addView(time);

            layoutT.addView(l);


        }
return checkBoxes;
    }
}

